package com.stools.reserva.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReservaInfoResponse {
    String vuelo;
    String nombre;
    String apellidos;
    Integer estadoReserva;
    Date fechaReserva;
    Date fechaVuelo;
}
