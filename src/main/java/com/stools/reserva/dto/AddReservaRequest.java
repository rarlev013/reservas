package com.stools.reserva.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddReservaRequest {
    Integer idUser;
    String nombrePasajero;
    String apellidoPaternoPasajero;
    String apellidoMaternoPasajero;
    String tipoDocPasajero;
    String numDocPasajero;
    String codVuelo;
}
