package com.stools.reserva.repository;

import com.stools.reserva.dto.ReservaInfoResponse;
import com.stools.reserva.model.TabReservas;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TabReservasRepository extends JpaRepository<TabReservas, Integer> {

    @Query(" SELECT COUNT(*) " +
            "FROM TabReservas " +
            "WHERE idVuelo = :idVuelo" +
            "   AND estado = :estado")
    Integer getCountReservasActivas(@Param("idVuelo") String idVuelo,
                                    @Param("estado") Integer estado);

    @Query(" SELECT new com.stools.reserva.dto.ReservaInfoResponse( " +
            "   TR.idVuelo, " +
            "   TR.nombre, " +
            "   TRIM(concat(IFNULL(TR.apellidoPaterno,''), ' ', IFNULL(TR.apellidoMaterno,''))), " +
            "   TR.estado, " +
            "   TR.fechaReserva, " +
            "   TV.fechaSalida) " +
            "FROM TabReservas TR " +
            "INNER JOIN TabVuelos TV ON TV.id = TR.idVuelo " +
            "WHERE TR.idUsuario = :idUsuario ")
    List<ReservaInfoResponse> getUserReservasInformation(Integer idUsuario);

}
