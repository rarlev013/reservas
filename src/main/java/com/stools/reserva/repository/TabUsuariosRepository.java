package com.stools.reserva.repository;

import com.stools.reserva.model.TabUsuarios;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TabUsuariosRepository extends JpaRepository<TabUsuarios, Integer> {


}
