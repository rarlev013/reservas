package com.stools.reserva.repository;

import com.stools.reserva.model.TabVuelos;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TabVuelosRepository extends JpaRepository<TabVuelos, String> {

}
