package com.stools.reserva.service.impl;

import com.stools.reserva.constant.AppConstant;
import com.stools.reserva.dto.AddReservaRequest;
import com.stools.reserva.dto.GeneralResponse;
import com.stools.reserva.dto.ReservaInfoResponse;
import com.stools.reserva.model.TabReservas;
import com.stools.reserva.model.TabVuelos;
import com.stools.reserva.repository.TabReservasRepository;
import com.stools.reserva.repository.TabVuelosRepository;
import com.stools.reserva.service.IReservaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class ReservaService implements IReservaService {

    private TabReservasRepository reservasRepository;
    private TabVuelosRepository vuelosRepository;


    public ReservaService(TabReservasRepository reservasRepository,
                          TabVuelosRepository vuelosRepository){
        this.reservasRepository = reservasRepository;
        this.vuelosRepository = vuelosRepository;
    }

    @Override
    public GeneralResponse createReserva(AddReservaRequest request) {
        GeneralResponse response = new GeneralResponse(true, 0, "", null);

        String estadoVuelo = this.validateCapacidadVuelo(request.getCodVuelo());

        if (!estadoVuelo.isEmpty()) {
            response.setSuccess(false);
            response.setMessage(estadoVuelo);
        }

        if (response.isSuccess()) {
            String responseInsertaVuelo = this.insertNuevaReserva(request.getIdUser(), request.getNombrePasajero() ,
                    request.getApellidoPaternoPasajero(), request.getApellidoMaternoPasajero(), request.getCodVuelo());

            if (!responseInsertaVuelo.isEmpty()) {
                log.error("Error en la creacion de reserva para el vuelo {}: {}",request.getCodVuelo(), responseInsertaVuelo);
                response.setSuccess(false);
                response.setMessage(AppConstant.ERROR_CREA_RESERVA);
            }
        }

        if (response.isSuccess()) {
            response.setMessage("Reserva creada existosamente");
        }

        return response;
    }

    @Override
    public List<ReservaInfoResponse> listarReservas(Integer idUsuario) {
        return reservasRepository.getUserReservasInformation(idUsuario);
    }

    @Override
    public String insertNuevaReserva(Integer idUsuario, String nombre, String apePaterno, String apeMaterno, String idVuelo) {
        String response = "";

        TabReservas tabReservas = new TabReservas();

        try {
            tabReservas.setIdVuelo(idVuelo);
            tabReservas.setIdUsuario(idUsuario);
            tabReservas.setNombre(nombre);
            tabReservas.setApellidoPaterno(apePaterno);
            tabReservas.setApellidoMaterno(apeMaterno);
            tabReservas.setFechaReserva(new Date());
            tabReservas.setEstado(AppConstant.RESERVA_ACTIVA);
            tabReservas.setConfirmado(AppConstant.RESERVA_NO_CONFIRMADA);

            reservasRepository.saveAndFlush(tabReservas);
        } catch (Exception e) {
            response = "error: " + e.getMessage();
        }

        return response;
    }

    @Override
    public String validateCapacidadVuelo(String codVuelo) {
        String response = "";

        try {
            int currNumReservas = reservasRepository.getCountReservasActivas(codVuelo, AppConstant.RESERVA_ACTIVA);
            Optional<TabVuelos> vueloFetch = vuelosRepository.findById(codVuelo);
            TabVuelos vuelo;

            if (vueloFetch.isPresent()) {
                vuelo = vueloFetch.get();

                if (vuelo.getCapacidad() <= currNumReservas) {
                    response = "Capacidad del vuelo " + codVuelo + "completa";
                }

            } else {
                response = "El vuelo " + codVuelo + " no existe";
            }
        } catch (Exception e){
            response = e.getMessage();
        }

        return response;
    }

}
