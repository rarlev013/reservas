package com.stools.reserva.service;

import com.stools.reserva.dto.AddReservaRequest;
import com.stools.reserva.dto.GeneralResponse;
import com.stools.reserva.dto.ReservaInfoResponse;

import java.util.List;

public interface IReservaService {

    GeneralResponse createReserva(AddReservaRequest request);
    List<ReservaInfoResponse> listarReservas(Integer idUsuario);
    String insertNuevaReserva(Integer idUsuario, String nombre, String apePaterno, String apeMaterno, String idVuelo);
    String validateCapacidadVuelo(String codVuelo);
}
