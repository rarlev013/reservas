package com.stools.reserva.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TabUsuarios {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "N_TAB_USUARIOS_ID")
    private Integer id;

    @Column(name = "C_CORREO")
    private String correo;

    @Column(name = "C_PASSWORD")
    private String password;

    @Column(name = "C_NOMBRE")
    private String nombre;

    @Column(name = "C_APELLIDO_PATERNO")
    private String apellidoPaterno;

    @Column(name = "C_APELLIDO_MATERNO")
    private String apellidoMaterno;

    @Column(name = "C_TIPO_DOCUMENTO")
    private String tipoDocumento;

    @Column(name = "C_NUM_DOCUMENTO")
    private String numDocumento;




}
