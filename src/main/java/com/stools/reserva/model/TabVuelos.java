package com.stools.reserva.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TabVuelos {

    @Id
    @Column(name = "C_TAB_VUELOS_ID")
    private String id;

    @Column(name = "C_CITY_DEPARTURE")
    private String ciudadSalida;

    @Column(name = "C_CITY_ARRIVAL")
    private String ciudadLlegada;

    @Column(name = "D_DATE_DEPARTURE")
    private Date fechaSalida;

    @Column(name = "D_DATE_ARRIVAL")
    private Date fechaLlegada;

    @Column(name = "N_CAPACITY")
    private Integer capacidad;

}
