package com.stools.reserva.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TabReservas {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "N_TAB_RESERVAS_ID")
    private Integer id;

    @Column(name = "C_TAB_VUELOS_ID")
    private String idVuelo;

    @Column(name = "N_TAB_USUARIOS_ID")
    private Integer idUsuario;

    @Column(name = "C_NOMBRE")
    private String nombre;

    @Column(name = "C_APELLIDO_PATERNO")
    private String apellidoPaterno;

    @Column(name = "C_APELLIDO_MATERNO")
    private String apellidoMaterno;

    @Column(name = "D_FECHA_RESERVA")
    private Date fechaReserva;

    @Column(name = "D_FECHA_FIN_RESERVA")
    private Date finReserva;

    @Column(name = "N_RESERVA_ACTIVA")
    private Integer estado;

    @Column(name = "N_CONFIRMADO")
    private Integer confirmado;


}
