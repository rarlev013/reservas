package com.stools.reserva.controller;

import com.stools.reserva.dto.AddReservaRequest;
import com.stools.reserva.dto.GeneralResponse;
import com.stools.reserva.dto.ReservaInfoResponse;
import com.stools.reserva.service.IReservaService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("reserva")
public class ReservaController {

    private IReservaService reservaService;

    public ReservaController(IReservaService reservaService){
        this.reservaService =  reservaService;
    }

    // TODO el valor idUsuario debe ser obtenido de la sesion
    @PostMapping("add")
    public ResponseEntity<?> addReservation(@RequestBody AddReservaRequest request) {

        GeneralResponse response = reservaService.createReserva(request);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("list")
    public ResponseEntity<?> listReservas() {

        GeneralResponse response = new GeneralResponse(true, 0, "", null);

        // TODO el valor idUsuario debe ser obtenido de la sesion
        Integer idUsuario = 1;

        List<ReservaInfoResponse> reservas = reservaService.listarReservas(idUsuario);
        response.setBody(reservas);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}

