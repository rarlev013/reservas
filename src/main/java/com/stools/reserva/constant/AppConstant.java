package com.stools.reserva.constant;

public class AppConstant {
    private AppConstant(){
    }

    public static final Integer RESERVA_ACTIVA = 1;
    public static final Integer RESERVA_INACTIVA = 0;
    public static final Integer RESERVA_CONFIRMADA = 1;
    public static final Integer RESERVA_NO_CONFIRMADA = 0;

    public static final String ERROR_CREA_RESERVA = "Hubo un error en la creacion de la reserva, por favor intente en unis minutos";
}
