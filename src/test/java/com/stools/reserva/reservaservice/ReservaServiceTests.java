package com.stools.reserva.reservaservice;

import com.stools.reserva.dto.AddReservaRequest;
import com.stools.reserva.dto.GeneralResponse;
import com.stools.reserva.dto.ReservaInfoResponse;
import com.stools.reserva.model.TabVuelos;
import com.stools.reserva.repository.TabReservasRepository;
import com.stools.reserva.repository.TabVuelosRepository;
import com.stools.reserva.service.impl.ReservaService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Slf4j
public class ReservaServiceTests {

    @Mock
    private TabReservasRepository reservasRepository;

    @Mock
    private TabVuelosRepository vuelosRepository;

    @InjectMocks
    private ReservaService reservaService;
    private AutoCloseable closeable;
    private static List<ReservaInfoResponse> reservaInfoResponses1;
    private static List<ReservaInfoResponse> reservaInfoResponses2;
    private static Optional<TabVuelos> vuelo;

    private static AddReservaRequest request;

    @BeforeEach
    void initService() {
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void closeService() throws Exception {
        closeable.close();
    }

    @BeforeAll
    static void setUpVariables() throws ParseException {

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);

        reservaInfoResponses1 = new ArrayList<>();
        reservaInfoResponses2 = new ArrayList<>();

        ReservaInfoResponse rir1 = new ReservaInfoResponse("0001AB", "JUAN", "GARCIA CHAVEZ", 1,
               formatter.parse("09-10-2023"), formatter.parse("25-10-2023"));
        ReservaInfoResponse rir2 = new ReservaInfoResponse("0001AC", "JORGE", "SANCHEZ GARCIA",1,
                formatter.parse("12-10-2023"), formatter.parse("30-10-2023"));

        reservaInfoResponses1.add(rir1);
        reservaInfoResponses1.add(rir2);
        reservaInfoResponses2.add(rir1);
        reservaInfoResponses2.add(rir2);

        TabVuelos tabVuelos = new TabVuelos();
        tabVuelos.setCapacidad(200);
        vuelo = Optional.of(tabVuelos);

        request = new AddReservaRequest();
        request.setIdUser(1);
        request.setCodVuelo("001H");
        request.setApellidoPaternoPasajero("Perez");
        request.setApellidoMaternoPasajero("Galvez");
        request.setNombrePasajero("Juan");

    }

    @DisplayName("Testing reserva exitosa")
    @Test
    void reservaExitosa_successCreation() {

        Mockito.when(
                reservasRepository.getCountReservasActivas(Mockito.anyString(), Mockito.anyInt())
        ).thenReturn(0);

        Mockito.when(
                vuelosRepository.findById(Mockito.anyString())
        ).thenReturn(vuelo);

        Mockito.when(
                reservasRepository.saveAndFlush(Mockito.any())
        ).thenReturn(null);

        GeneralResponse res = reservaService.createReserva(request);

        Assertions.assertNotNull(res);
        Assertions.assertTrue(res.isSuccess());
    }

    @DisplayName("Testing reserva fallida, error insercion de registro nuevo")
    @Test
    void reservaFallida_failRecordInsertion() {

        Mockito.when(
                reservasRepository.getCountReservasActivas(Mockito.anyString(), Mockito.anyInt())
        ).thenReturn(0);
        Mockito.when(
                vuelosRepository.findById(Mockito.anyString())
        ).thenReturn(vuelo);

        Mockito.when(
                reservasRepository.saveAndFlush(Mockito.any())
        ).thenThrow(RuntimeException.class);

        GeneralResponse res = reservaService.createReserva(request);

        Assertions.assertNotNull(res);
        Assertions.assertFalse(res.isSuccess());
    }

    @DisplayName("Testing reserva fallida, error verificacion capacidad")
    @Test
    void reservaFallida_failCApacityCheck() {

        Mockito.when(
                reservasRepository.getCountReservasActivas(Mockito.anyString(), Mockito.anyInt())
        ).thenReturn(300);
        Mockito.when(
                vuelosRepository.findById(Mockito.anyString())
        ).thenReturn(vuelo);

        Mockito.when(
                reservasRepository.saveAndFlush(Mockito.any())
        ).thenReturn(null);

        GeneralResponse res = reservaService.createReserva(request);

        Assertions.assertNotNull(res);
        Assertions.assertFalse(res.isSuccess());
    }

    @DisplayName("Testing listar reservas")
    @Test
    void listReservar_shouldReturnNotEmpty() {

        Mockito.when(
                reservasRepository.getUserReservasInformation(Mockito.anyInt())
        ).thenReturn(reservaInfoResponses1);

        List<ReservaInfoResponse> responses = reservaService.listarReservas(1);
        Assertions.assertEquals(reservaInfoResponses2, responses);
    }

    @DisplayName("Testing listar reservas con usuario invalido")
    @Test
    void listReservar_shouldReturnEmpty() {
        List<ReservaInfoResponse> responses = reservaService.listarReservas(-1);
        Assertions.assertEquals(0, responses.size());
    }

}
